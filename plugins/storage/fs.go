package storage

import (
	"os"
	"strconv"
	"strings"
)

type Volume struct {
	BackingDevice string
	Filesystem    string
	Name          string
	Options       string
	Virtual       bool
	Blocks        struct {
		Size  int64
		Total uint64
		Free  uint64
		Avail uint64
	}
}

type LogicalDisk struct {
	Name       string // Name - /dev/sda
	Vendor     string // Vendor - "ATA"
	Product    string // Product - "INTEL SSDSC2BB12"
	BusInfo    string // BusInfo - "scsi@0:0.0.0"
	Dev        string // Dev - "8:0"
	Size       int64  // Size - 120034123776
	Serial     string // Serial
	Removable  bool   // Removable
	ReadOnly   bool   // ReadOnly
	Rotational bool   // Rotational
	ByID       string // Symlink based on ID
	ByPath     string // Symlink based on path
}

type Info struct {
	Volumes     []Volume
	Disks       []LogicalDisk
	Controllers []interface{}
}

func (i *Info) Class() string {
	return "Storage"
}

func getStringFromFile(file, def string) string {
	data, err := os.ReadFile(file)
	if err != nil {
		return def
	}
	return strings.TrimSpace(string(data))
}

func getInt64FromFile(file string, def int64) int64 {
	s := getStringFromFile(file, "BAD")
	if s == "BAD" {
		return def
	}
	a, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return def
	}
	return a
}

// assumes 0 is false and other is true
func getBoolFromFile(file string, def bool) bool {
	idef := int64(0)
	if def {
		idef = 1
	}
	if ii := getInt64FromFile(file, idef); ii == 0 {
		return false
	}
	return true
}

func Gather() (*Info, error) {
	res := &Info{
		Volumes:     []Volume{},
		Disks:       []LogicalDisk{},
		Controllers: []interface{}{},
	}
	err := res.Fill()
	return res, err
}
