//go:build linux
// +build linux

package storage

import (
	"bufio"
	"encoding/binary"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"syscall"

	"gitlab.com/rackn/gohai/plugins/utils"
)

// At some point, also need to add mode block device oriented information here
const (
	dirByIdPath   = "/dev/disk/by-id"
	dirByPathPath = "/dev/disk/by-path"
)

// buildSymlinkMap reads the directory at dirPath and builds a map of symlinks
func buildSymlinkMap(dirPath string) map[string]string {
	symlinkMap := make(map[string]string)
	entries, err := os.ReadDir(dirPath)
	if err == nil {
		for _, entry := range entries {
			entryPath := filepath.Join(dirPath, entry.Name())
			info, err := entry.Info()
			if err != nil {
				fmt.Println("Error getting info:", err)
				continue
			}

			if info.Mode()&os.ModeSymlink != 0 {
				linkTarget, err := os.Readlink(entryPath)
				if err != nil {
					fmt.Println("Error reading link:", err)
					continue
				}
				symlinkMap[filepath.Base(linkTarget)] = entryPath
			}
		}
	}
	return symlinkMap
}

func (res *Info) Fill() error {
	mounts, err := os.Open("/proc/self/mounts")
	if err != nil {
		return err
	}
	defer mounts.Close()
	mountLines := bufio.NewScanner(mounts)
	for mountLines.Scan() {
		line := mountLines.Text()
		fields := strings.Split(line, " ")
		if len(fields) != 6 {
			continue
		}
		vol := Volume{
			Name:          fields[1],
			BackingDevice: fields[0],
			Filesystem:    fields[2],
			Options:       fields[3],
		}
		stat, err := os.Stat(vol.BackingDevice)
		if err == nil {
			vol.Virtual = !(stat.Mode()&os.ModeDevice > 0)
		}
		fsStat := &syscall.Statfs_t{}
		if err := syscall.Statfs(vol.Name, fsStat); err == nil {
			vol.Blocks.Size = int64(fsStat.Bsize)
			vol.Blocks.Total = fsStat.Blocks
			vol.Blocks.Free = fsStat.Bfree
			vol.Blocks.Avail = fsStat.Bavail
		}
		res.Volumes = append(res.Volumes, vol)
	}

	byIdMap := buildSymlinkMap(dirByIdPath)
	byPathMap := buildSymlinkMap(dirByPathPath)

	files, err := os.ReadDir("/sys/block")
	if err == nil {
		disks := []LogicalDisk{}
		for _, fi := range files {
			file := fi.Name()
			_, err := os.Stat(fmt.Sprintf("/sys/block/%s/device", file))
			if os.IsNotExist(err) {
				continue
			}
			dtype := getInt64FromFile(fmt.Sprintf("/sys/block/%s/device/type", file), 0)
			switch dtype {
			case 0, 12, 13, 7:
				// These are good.
			default:
				continue
			}

			disk := LogicalDisk{}
			disk.Name = fmt.Sprintf("/dev/%s", file)
			disk.Removable = getBoolFromFile(fmt.Sprintf("/sys/block/%s/removable", file), false)
			disk.ReadOnly = getBoolFromFile(fmt.Sprintf("/sys/block/%s/ro", file), false)
			disk.Rotational = getBoolFromFile(fmt.Sprintf("/sys/block/%s/queue/rotational", file), false)
			disk.Dev = getStringFromFile(fmt.Sprintf("/sys/block/%s/dev", file), "0:0")
			ii := getInt64FromFile(fmt.Sprintf("/sys/block/%s/size", file), 0)
			disk.Size = ii * 512
			disk.Product = getStringFromFile(fmt.Sprintf("/sys/block/%s/device/model", file), "UNKNOWN")
			disk.Vendor = getStringFromFile(fmt.Sprintf("/sys/block/%s/device/vendor", file), "UNKNOWN")
			disk.ByID = byIdMap[file]
			disk.ByPath = byPathMap[file]

			dir, err := os.Readlink(fmt.Sprintf("/sys/block/%s", file))
			if err != nil {
				dir = "../devices/pci/UNKNOWN"
			}
			parts := strings.Split(dir, "/")
			if len(parts) < 4 {
				disk.BusInfo = "UNKNOWN"
			} else {
				answer := parts[3]
				if strings.Contains(parts[2], "pci") {
					answer = fmt.Sprintf("pci@%s", parts[3])
				}
				disk.BusInfo = answer
			}

			data, err := os.ReadFile(fmt.Sprintf("/sys/block/%s/device/vpd_pg80", file))
			if err == nil {
				len := binary.BigEndian.Uint16(data[2:4])
				disk.Serial = strings.TrimSpace(string(data[4 : 4+len]))
			} else {
				disk.Serial = getStringFromFile(fmt.Sprintf("/sys/block/%s/device/serial", file), "UNKNOWN")
			}
			disks = append(disks, disk)
		}
		res.Disks = disks
	}

	tmpC, err := utils.GetLSHWPiece("storage")
	if tmpC != nil && err == nil {
		res.Controllers = tmpC
	}
	return err
}
