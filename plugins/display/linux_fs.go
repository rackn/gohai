package display

import (
	"gitlab.com/rackn/gohai/plugins/utils"
)

type Info struct {
	Controllers []interface{}
}

func (i *Info) Class() string {
	return "Display"
}

func Gather() (*Info, error) {
	res := &Info{
		Controllers: []interface{}{},
	}
	tmpC, err := utils.GetLSHWPiece("display")
	if tmpC != nil && err == nil {
		res.Controllers = tmpC
	}
	return res, err
}
