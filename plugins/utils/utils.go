package utils

import (
	"encoding/json"
	"fmt"
	"os/exec"
	"regexp"
	"strings"
)

var missingComma = regexp.MustCompile(`\n[ \t]*}[ \t]*{[ \t]*\n`)
var trailingComma = regexp.MustCompile(`},$`)

func GetLSHWPiece(class string) ([]interface{}, error) {
	// We have lshw - use it.
	if _, err := exec.Command("lshw", "--help").CombinedOutput(); err != nil {
		return nil, nil
	}

	out, err := exec.Command("lshw", "-quiet", "-c", class, "-json").CombinedOutput()
	if err != nil {
		return nil, err
	}

	objs := []interface{}{}

	// Sometimes it doesn't have a wrapping array parts
	sout := string(out)
	sout = strings.TrimSpace(sout)
	sout = trailingComma.ReplaceAllString(sout, "}")
	if len(sout) == 0 || sout[0] != '[' {
		sout = fmt.Sprintf("[%s]", sout)
	}
	// Sometimes it misses commas
	sout = missingComma.ReplaceAllString(sout, "\n},{\n")

	err = json.Unmarshal([]byte(sout), &objs)
	if err != nil {
		return nil, err
	}

	return objs, nil
}
