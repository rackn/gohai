//go:build !linux
// +build !linux

package net

func (i *Interface) Fill() error {
	// This isn't true, but windows doesn't know
	if i.HardwareAddr.String() != "" {
		i.Sys.IsPhysical = true
	}
	return nil
}
