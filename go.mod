module gitlab.com/rackn/gohai

go 1.20

require github.com/VictorLowther/godmi v0.6.1

require github.com/digitalocean/go-smbios v0.0.0-20180907143718-390a4f403a8e // indirect
